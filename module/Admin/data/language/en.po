msgid ""
msgstr ""
"Project-Id-Version: DBErp\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-04-12 00:43+0800\n"
"PO-Revision-Date: 2019-04-12 00:43+0800\n"
"Last-Translator: Baron <baron@loongdom.cn>\n"
"Language-Team: DBErp <baron@loongdom.cn>\n"
"Language: en\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-KeywordsList: translate\n"
"X-Poedit-Basepath: .\n"
"X-Generator: Poedit 2.0.7\n"
"X-Poedit-SourceCharset: UTF-8\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Poedit-SearchPath-0: ../..\n"

#: ../../src/Controller/AdminController.php:92
#, php-format
msgid "管理员 %s 添加成功！"
msgstr ""

#: ../../src/Controller/AdminController.php:93
#: ../../src/Controller/AdminController.php:130
#: ../../src/Controller/AdminController.php:164
#: ../../src/Controller/AdminController.php:197
#: ../../view/admin/admin/index.phtml:25 ../../view/layout/left-menu.phtml:32
#: ../../view/partial/breadcrumb.phtml:12
msgid "管理员"
msgstr ""

#: ../../src/Controller/AdminController.php:113
#: ../../src/Controller/AdminController.php:150
#: ../../src/Controller/AdminController.php:190
msgid "该管理员不存在！"
msgstr ""

#: ../../src/Controller/AdminController.php:129
#, php-format
msgid "管理员 %s 编辑成功！"
msgstr ""

#: ../../src/Controller/AdminController.php:163
#, php-format
msgid "管理员 %s 密码修改成功！"
msgstr ""

#: ../../src/Controller/AdminController.php:184
msgid "不能删除创始人！"
msgstr ""

#: ../../src/Controller/AdminController.php:196
#, php-format
msgid "管理员 %s 删除成功！"
msgstr ""

#: ../../src/Controller/AdminGroupController.php:95
#, php-format
msgid "管理员组 %s 添加成功！"
msgstr ""

#: ../../src/Controller/AdminGroupController.php:96
#: ../../src/Controller/AdminGroupController.php:143
#: ../../src/Controller/AdminGroupController.php:177
#: ../../view/admin/admin-group/add-admin-group.phtml:34
#: ../../view/admin/admin-group/admin-group-list.phtml:25
#: ../../view/admin/admin/add.phtml:50 ../../view/layout/left-menu.phtml:37
#: ../../view/partial/breadcrumb.phtml:33
msgid "管理员组"
msgstr ""

#: ../../src/Controller/AdminGroupController.php:117
#: ../../src/Controller/AdminGroupController.php:169
msgid "该管理员组不存在！"
msgstr ""

#: ../../src/Controller/AdminGroupController.php:142
#, php-format
msgid "管理员组 %s 编辑成功！"
msgstr ""

#: ../../src/Controller/AdminGroupController.php:163
msgid "该管理员组不能删除！"
msgstr ""

#: ../../src/Controller/AdminGroupController.php:176
#, php-format
msgid "管理员组 %s 删除成功！"
msgstr ""

#: ../../src/Controller/AdminGroupController.php:180
#, php-format
msgid "管理员组 %s 删除失败！该组不存在或该组下有管理员"
msgstr ""

#: ../../src/Controller/AppController.php:69
#, php-format
msgid "应用 %s 添加成功！"
msgstr ""

#: ../../src/Controller/AppController.php:70
#: ../../src/Controller/AppController.php:105
#: ../../src/Controller/AppController.php:137
#: ../../view/layout/left-menu.phtml:47 ../../view/partial/breadcrumb.phtml:69
msgid "商城绑定"
msgstr ""

#: ../../src/Controller/AppController.php:91
#: ../../src/Controller/AppController.php:130
msgid "该应用不存在！"
msgstr ""

#: ../../src/Controller/AppController.php:104
#, php-format
msgid "应用 %s 编辑成功！"
msgstr ""

#: ../../src/Controller/AppController.php:136
#, php-format
msgid "应用 %s 删除成功！"
msgstr ""

#: ../../src/Controller/IndexController.php:82
msgid "管理员登录"
msgstr ""

#: ../../src/Controller/OperLogController.php:52
#: ../../view/admin/oper-log/index.phtml:32
msgid "操作者级别"
msgstr ""

#: ../../src/Controller/OperLogController.php:81
msgid "操作记录删除成功！"
msgstr ""

#: ../../src/Controller/OperLogController.php:82
#: ../../view/layout/left-menu.phtml:52 ../../view/partial/breadcrumb.phtml:87
msgid "操作日志"
msgstr ""

#: ../../src/Controller/RegionController.php:103
msgid "地区添加成功！"
msgstr ""

#: ../../src/Controller/RegionController.php:104
#: ../../src/Controller/RegionController.php:152
#: ../../src/Controller/RegionController.php:179
#: ../../src/Controller/RegionController.php:212
#: ../../view/layout/left-menu.phtml:42 ../../view/partial/breadcrumb.phtml:51
msgid "地区管理"
msgstr ""

#: ../../src/Controller/RegionController.php:115
msgid "地区数据最多添加三级！"
msgstr ""

#: ../../src/Controller/RegionController.php:137
#: ../../src/Controller/RegionController.php:199
msgid "该地区不存在！"
msgstr ""

#: ../../src/Controller/RegionController.php:151
#, php-format
msgid "地区 %s 编辑成功！"
msgstr ""

#: ../../src/Controller/RegionController.php:178
msgid "批量处理成功！"
msgstr ""

#: ../../src/Controller/RegionController.php:205
#, php-format
msgid "地区 %s 删除失败！其下级还有其他地区！"
msgstr ""

#: ../../src/Controller/RegionController.php:211
#, php-format
msgid "地区 %s 删除成功！"
msgstr ""

#: ../../src/Data/Common.php:28 ../../view/admin/admin/add.phtml:95
#: ../../view/admin/admin/index.phtml:93 ../../view/admin/app/add.phtml:112
#: ../../view/admin/app/index.phtml:36
msgid "启用"
msgstr ""

#: ../../src/Data/Common.php:28 ../../view/admin/admin/index.phtml:93
#: ../../view/admin/app/index.phtml:36
msgid "禁用"
msgstr ""

#: ../../src/Data/Common.php:38
msgid "DBShop商城系统"
msgstr ""

#: ../../src/Data/Common.php:38
msgid "其他系统"
msgstr ""

#: ../../src/Data/Common.php:50 ../../src/Data/Common.php:69
#: ../../src/Data/Common.php:98 ../../src/Data/Common.php:117
msgid "已退货"
msgstr ""

#: ../../src/Data/Common.php:51 ../../src/Data/Common.php:99
msgid "退货"
msgstr ""

#: ../../src/Data/Common.php:52
msgid "未审核"
msgstr ""

#: ../../src/Data/Common.php:53
msgid "已审核"
msgstr ""

#: ../../src/Data/Common.php:54
msgid "待入库"
msgstr ""

#: ../../src/Data/Common.php:55
msgid "已入库"
msgstr ""

#: ../../src/Data/Common.php:68 ../../src/Data/Common.php:116
msgid "退货中"
msgstr ""

#: ../../src/Data/Common.php:82
msgid "付款方式"
msgstr ""

#: ../../src/Data/Common.php:83 ../../view/layout/left-menu.phtml:216
#: ../../view/partial/breadcrumb.phtml:358
msgid "应付账款"
msgstr ""

#: ../../src/Data/Common.php:84
msgid "现金付款"
msgstr ""

#: ../../src/Data/Common.php:85
msgid "预付款"
msgstr ""

#: ../../src/Data/Common.php:100
msgid "待确认"
msgstr ""

#: ../../src/Data/Common.php:101
msgid "已确认"
msgstr ""

#: ../../src/Data/Common.php:102
msgid "发货出库"
msgstr ""

#: ../../src/Data/Common.php:103
msgid "确认收货"
msgstr ""

#: ../../src/Data/Common.php:130
msgid "收款方式"
msgstr ""

#: ../../src/Data/Common.php:131 ../../view/layout/left-menu.phtml:220
#: ../../view/partial/breadcrumb.phtml:375
msgid "应收账款"
msgstr ""

#: ../../src/Data/Common.php:132
msgid "现金收款"
msgstr ""

#: ../../src/Data/Common.php:133
msgid "预存款"
msgstr ""

#: ../../src/Data/Common.php:145
msgid "验货完成等待入库"
msgstr ""

#: ../../src/Data/Common.php:146
msgid "验货完成直接入库"
msgstr ""

#: ../../src/Data/Common.php:158
msgid "无"
msgstr ""

#: ../../src/Data/Common.php:159
msgid "有"
msgstr ""

#: ../../src/Data/Common.php:172
msgid "已取消"
msgstr ""

#: ../../src/Data/Common.php:173
msgid "待付款"
msgstr ""

#: ../../src/Data/Common.php:174
msgid "付款中"
msgstr ""

#: ../../src/Data/Common.php:175
msgid "已付款"
msgstr ""

#: ../../src/Data/Common.php:176
msgid "待发货"
msgstr ""

#: ../../src/Data/Common.php:177
msgid "已发货"
msgstr ""

#: ../../src/Data/Common.php:178
msgid "订单完成"
msgstr ""

#: ../../src/Form/SearchAdminForm.php:46 ../../src/Form/SearchRegionForm.php:47
msgid "起始ID"
msgstr ""

#: ../../src/Form/SearchAdminForm.php:56 ../../src/Form/SearchRegionForm.php:57
msgid "结束ID"
msgstr ""

#: ../../src/Form/SearchAdminForm.php:66
msgid "管理员名称"
msgstr ""

#: ../../src/Form/SearchAdminForm.php:76 ../../view/admin/admin/add.phtml:18
#: ../../view/admin/admin/add.phtml:66 ../../view/admin/admin/index.phtml:26
msgid "电子邮箱"
msgstr ""

#: ../../src/Form/SearchAdminForm.php:110
#: ../../src/Form/SearchOperLogForm.php:84
msgid "起始时间"
msgstr ""

#: ../../src/Form/SearchAdminForm.php:120
#: ../../src/Form/SearchOperLogForm.php:94
msgid "结束时间"
msgstr ""

#: ../../src/Form/SearchOperLogForm.php:45
#: ../../view/admin/oper-log/index.phtml:31
msgid "操作者"
msgstr ""

#: ../../src/Form/SearchOperLogForm.php:64
#: ../../view/admin/oper-log/index.phtml:34
msgid "ip地址"
msgstr ""

#: ../../src/Form/SearchOperLogForm.php:74
#: ../../view/admin/oper-log/index.phtml:35
msgid "操作描述"
msgstr ""

#: ../../src/Form/SearchRegionForm.php:67 ../../view/admin/region/add.phtml:7
#: ../../view/admin/region/add.phtml:41 ../../view/admin/region/index.phtml:36
msgid "地区名称"
msgstr ""

#: ../../src/Plugin/AdminCommonPlugin.php:61
msgid "不正确的请求！"
msgstr ""

#: ../../src/Plugin/AdminCommonPlugin.php:141
msgid "选择商城"
msgstr ""

#: ../../src/Plugin/AdminCommonPlugin.php:158
msgid "选择管理组"
msgstr ""

#: ../../view/admin/admin-group/add-admin-group.phtml:7
msgid "管理员组名称"
msgstr ""

#: ../../view/admin/admin-group/add-admin-group.phtml:16
msgid "返回管理员组列表"
msgstr ""

#: ../../view/admin/admin-group/add-admin-group.phtml:17
#: ../../view/admin/admin-group/add-admin-group.phtml:85
msgid "保存管理员组"
msgstr ""

#: ../../view/admin/admin-group/add-admin-group.phtml:28
#: ../../view/partial/breadcrumb.phtml:43
msgid "编辑管理员组"
msgstr ""

#: ../../view/admin/admin-group/add-admin-group.phtml:28
#: ../../view/admin/admin-group/admin-group-list.phtml:7
#: ../../view/partial/breadcrumb.phtml:40
msgid "添加管理员组"
msgstr ""

#: ../../view/admin/admin-group/add-admin-group.phtml:43
msgid "权限设置"
msgstr ""

#: ../../view/admin/admin-group/add-admin-group.phtml:46
#: ../../view/admin/admin-group/add-admin-group.phtml:58
#: ../../view/admin/region/index.phtml:107
msgid "全选"
msgstr ""

#: ../../view/admin/admin-group/add-admin-group.phtml:107
msgid "管理员组不能为空！"
msgstr ""

#: ../../view/admin/admin-group/admin-group-list.phtml:26
#: ../../view/admin/admin/index.phtml:30 ../../view/admin/app/index.phtml:28
#: ../../view/admin/region/index.phtml:38
msgid "操作"
msgstr ""

#: ../../view/admin/admin-group/admin-group-list.phtml:37
#: ../../view/admin/admin/index.phtml:98 ../../view/admin/app/index.phtml:41
#: ../../view/admin/region/index.phtml:86
msgid "编辑"
msgstr ""

#: ../../view/admin/admin-group/admin-group-list.phtml:39
msgid "您确实要删除该管理员组吗？"
msgstr ""

#: ../../view/admin/admin-group/admin-group-list.phtml:39
#: ../../view/admin/admin/index.phtml:103 ../../view/admin/app/index.phtml:45
#: ../../view/admin/region/index.phtml:97
msgid "删除"
msgstr ""

#: ../../view/admin/admin/add.phtml:9 ../../view/admin/admin/add.phtml:58
#: ../../view/admin/admin/change-password.phtml:36
#: ../../view/admin/system/index.phtml:27
msgid "管理员账号"
msgstr ""

#: ../../view/admin/admin/add.phtml:21 ../../view/admin/admin/add.phtml:75
#: ../../view/admin/admin/change-password.phtml:7
#: ../../view/admin/admin/change-password.phtml:42
#: ../../view/admin/system/index.phtml:33
msgid "管理员密码"
msgstr ""

#: ../../view/admin/admin/add.phtml:24 ../../view/admin/admin/add.phtml:82
#: ../../view/admin/admin/change-password.phtml:10
#: ../../view/admin/admin/change-password.phtml:49
#: ../../view/admin/system/index.phtml:38
msgid "确认密码"
msgstr ""

#: ../../view/admin/admin/add.phtml:32
#: ../../view/admin/admin/change-password.phtml:18
msgid "返回管理员列表"
msgstr ""

#: ../../view/admin/admin/add.phtml:33 ../../view/admin/admin/add.phtml:107
msgid "保存管理员信息"
msgstr ""

#: ../../view/admin/admin/add.phtml:44 ../../view/partial/breadcrumb.phtml:22
msgid "编辑管理员"
msgstr ""

#: ../../view/admin/admin/add.phtml:44 ../../view/admin/admin/index.phtml:7
#: ../../view/partial/breadcrumb.phtml:19
msgid "添加管理员"
msgstr ""

#: ../../view/admin/admin/add.phtml:91 ../../view/admin/admin/index.phtml:28
#: ../../view/admin/app/add.phtml:108 ../../view/admin/app/index.phtml:26
msgid "状态"
msgstr ""

#: ../../view/admin/admin/add.phtml:146 ../../view/admin/admin/add.phtml:147
msgid "请选择管理员组"
msgstr ""

#: ../../view/admin/admin/add.phtml:150
msgid "请输入管理员邮箱"
msgstr ""

#: ../../view/admin/admin/add.phtml:151
msgid "请输入正确的邮箱地址"
msgstr ""

#: ../../view/admin/admin/add.phtml:156
msgid "请输入管理员账号"
msgstr ""

#: ../../view/admin/admin/add.phtml:159
#: ../../view/admin/admin/change-password.phtml:85
msgid "请输入管理员密码"
msgstr ""

#: ../../view/admin/admin/add.phtml:162
#: ../../view/admin/admin/change-password.phtml:88
msgid "请输入确认密码"
msgstr ""

#: ../../view/admin/admin/add.phtml:163
#: ../../view/admin/admin/change-password.phtml:89
msgid "两次输入的密码不一致"
msgstr ""

#: ../../view/admin/admin/change-password.phtml:19
#: ../../view/admin/admin/change-password.phtml:60
#: ../../view/admin/admin/index.phtml:101
#: ../../view/partial/breadcrumb.phtml:28
msgid "修改密码"
msgstr ""

#: ../../view/admin/admin/change-password.phtml:30
msgid "修改管理员密码"
msgstr ""

#: ../../view/admin/admin/index.phtml:27
msgid "管理组"
msgstr ""

#: ../../view/admin/admin/index.phtml:29 ../../view/admin/app/index.phtml:27
msgid "添加时间"
msgstr ""

#: ../../view/admin/admin/index.phtml:103
msgid "您确实要删除该账户吗？"
msgstr ""

#: ../../view/admin/app/add.phtml:7 ../../view/admin/app/add.phtml:60
#: ../../view/admin/app/index.phtml:24
msgid "商城名称"
msgstr ""

#: ../../view/admin/app/add.phtml:11 ../../view/admin/app/add.phtml:68
msgid "appId"
msgstr ""

#: ../../view/admin/app/add.phtml:15 ../../view/admin/app/add.phtml:81
msgid "appSecret"
msgstr ""

#: ../../view/admin/app/add.phtml:19
msgid "以https://或http://开头"
msgstr ""

#: ../../view/admin/app/add.phtml:23
msgid "端口"
msgstr ""

#: ../../view/admin/app/add.phtml:33
msgid "返回商城列表"
msgstr ""

#: ../../view/admin/app/add.phtml:34 ../../view/admin/app/add.phtml:124
msgid "保存商城信息"
msgstr ""

#: ../../view/admin/app/add.phtml:45
msgid "编辑商城"
msgstr ""

#: ../../view/admin/app/add.phtml:45 ../../view/admin/app/index.phtml:7
#: ../../view/partial/breadcrumb.phtml:76
msgid "添加商城"
msgstr ""

#: ../../view/admin/app/add.phtml:52
msgid "商城类别"
msgstr ""

#: ../../view/admin/app/add.phtml:75 ../../view/admin/app/add.phtml:88
msgid "点击系统生成"
msgstr ""

#: ../../view/admin/app/add.phtml:94
msgid "商城地址"
msgstr ""

#: ../../view/admin/app/add.phtml:163
msgid "商城名称不能为空！"
msgstr ""

#: ../../view/admin/app/add.phtml:166
msgid "appId不能为空！"
msgstr ""

#: ../../view/admin/app/add.phtml:169
msgid "APPSecret不能为空！"
msgstr ""

#: ../../view/admin/app/add.phtml:172
msgid "商城url地址不能为空！"
msgstr ""

#: ../../view/admin/app/add.phtml:175
msgid "端口不能为空！"
msgstr ""

#: ../../view/admin/app/add.phtml:176
msgid "必须为数字！"
msgstr ""

#: ../../view/admin/app/add.phtml:177
msgid "最小为1！"
msgstr ""

#: ../../view/admin/app/add.phtml:180
msgid "商城类别不能为空！"
msgstr ""

#: ../../view/admin/app/index.phtml:25
msgid "商城类型"
msgstr ""

#: ../../view/admin/app/index.phtml:44
msgid "您确实要删除该商城吗？"
msgstr ""

#: ../../view/admin/index/index.phtml:33
msgid "输入账号和密码进行登录"
msgstr ""

#: ../../view/admin/index/index.phtml:37
msgid "账号或密码错误"
msgstr ""

#: ../../view/admin/index/index.phtml:45
msgid "账号"
msgstr ""

#: ../../view/admin/index/index.phtml:48
msgid "密码"
msgstr ""

#: ../../view/admin/index/index.phtml:51
msgid "输入上面的验证码"
msgstr ""

#: ../../view/admin/index/index.phtml:54
msgid "登 录"
msgstr ""

#: ../../view/admin/index/index.phtml:88
msgid "记住我"
msgstr ""

#: ../../view/admin/index/index.phtml:114
msgid "请输入账号"
msgstr ""

#: ../../view/admin/index/index.phtml:119
msgid "请输入密码"
msgstr ""

#: ../../view/admin/index/index.phtml:124
msgid "请输入验证码"
msgstr ""

#: ../../view/admin/oper-log/index.phtml:8
msgid "选择清除时间"
msgstr ""

#: ../../view/admin/oper-log/index.phtml:9
msgid "一周前"
msgstr ""

#: ../../view/admin/oper-log/index.phtml:10
msgid "一个月前"
msgstr ""

#: ../../view/admin/oper-log/index.phtml:11
msgid "三个月前"
msgstr ""

#: ../../view/admin/oper-log/index.phtml:12
msgid "半年前"
msgstr ""

#: ../../view/admin/oper-log/index.phtml:14
msgid "删除日志"
msgstr ""

#: ../../view/admin/oper-log/index.phtml:33
msgid "操作时间"
msgstr ""

#: ../../view/admin/region/add.phtml:7
msgid "，可同时添加多个地区，一行一个"
msgstr ""

#: ../../view/admin/region/add.phtml:11 ../../view/admin/region/add.phtml:49
#: ../../view/admin/region/index.phtml:37
msgid "排序"
msgstr ""

#: ../../view/admin/region/add.phtml:23
msgid "返回地区列表"
msgstr ""

#: ../../view/admin/region/add.phtml:24 ../../view/admin/region/add.phtml:62
msgid "保存地区"
msgstr ""

#: ../../view/admin/region/add.phtml:35 ../../view/partial/breadcrumb.phtml:61
msgid "编辑地区"
msgstr ""

#: ../../view/admin/region/add.phtml:35 ../../view/admin/region/index.phtml:15
#: ../../view/partial/breadcrumb.phtml:58
msgid "添加地区"
msgstr ""

#: ../../view/admin/region/add.phtml:89
msgid "地区名称不能为空！"
msgstr ""

#: ../../view/admin/region/add.phtml:92
msgid "排序不能为空！"
msgstr ""

#: ../../view/admin/region/add.phtml:93
msgid "排序必须为数字！"
msgstr ""

#: ../../view/admin/region/add.phtml:94
msgid "排序最小为1！"
msgstr ""

#: ../../view/admin/region/index.phtml:9
msgid "返回顶级列表"
msgstr ""

#: ../../view/admin/region/index.phtml:10
msgid "返回上一级列表"
msgstr ""

#: ../../view/admin/region/index.phtml:92
msgid "查看下级"
msgstr ""

#: ../../view/admin/region/index.phtml:96
msgid "您确实要删除该地区吗？"
msgstr ""

#: ../../view/admin/region/index.phtml:109
msgid "选择状态"
msgstr ""

#: ../../view/admin/region/index.phtml:110
msgid "更新排序"
msgstr ""

#: ../../view/admin/region/index.phtml:111
msgid "地区删除"
msgstr ""

#: ../../view/admin/region/index.phtml:113
msgid "更新"
msgstr ""

#: ../../view/admin/system/index.phtml:7 ../../view/admin/system/index.phtml:69
msgid "保存设置"
msgstr ""

#: ../../view/layout/header.phtml:19 ../../view/layout/left-menu.phtml:19
#: ../../view/partial/breadcrumb.phtml:8
msgid "系统"
msgstr ""

#: ../../view/layout/header.phtml:22 ../../view/layout/left-menu.phtml:60
msgid "商品&仓库"
msgstr ""

#: ../../view/layout/header.phtml:25 ../../view/layout/left-menu.phtml:111
#: ../../view/partial/breadcrumb.phtml:217
msgid "采购"
msgstr ""

#: ../../view/layout/header.phtml:28 ../../view/layout/left-menu.phtml:137
#: ../../view/partial/breadcrumb.phtml:394
msgid "销售"
msgstr ""

#: ../../view/layout/header.phtml:31 ../../view/layout/left-menu.phtml:178
#: ../../view/layout/left-menu.phtml:186
#: ../../view/partial/breadcrumb.phtml:276
#: ../../view/partial/breadcrumb.phtml:280
msgid "客户"
msgstr ""

#: ../../view/layout/header.phtml:34 ../../view/layout/left-menu.phtml:162
#: ../../view/partial/breadcrumb.phtml:447
msgid "商城"
msgstr ""

#: ../../view/layout/header.phtml:37 ../../view/layout/left-menu.phtml:209
#: ../../view/partial/breadcrumb.phtml:354
msgid "资金"
msgstr ""

#: ../../view/layout/header.phtml:40 ../../view/layout/left-menu.phtml:237
msgid "报表"
msgstr ""

#: ../../view/layout/header.phtml:248
msgid "官方网站"
msgstr ""

#: ../../view/layout/header.phtml:251
msgid "交流论坛"
msgstr ""

#: ../../view/layout/header.phtml:254
msgid "在线文档"
msgstr ""

#: ../../view/layout/left-menu.phtml:27
msgid "系统设置"
msgstr ""

#: ../../view/layout/left-menu.phtml:68 ../../view/partial/breadcrumb.phtml:102
msgid "商品"
msgstr ""

#: ../../view/layout/left-menu.phtml:73 ../../view/partial/breadcrumb.phtml:126
msgid "商品分类"
msgstr ""

#: ../../view/layout/left-menu.phtml:78 ../../view/partial/breadcrumb.phtml:180
msgid "计量单位"
msgstr ""

#: ../../view/layout/left-menu.phtml:83 ../../view/partial/breadcrumb.phtml:144
msgid "商品品牌"
msgstr ""

#: ../../view/layout/left-menu.phtml:88 ../../view/partial/breadcrumb.phtml:98
#: ../../view/partial/breadcrumb.phtml:198
msgid "仓库"
msgstr ""

#: ../../view/layout/left-menu.phtml:93 ../../view/partial/breadcrumb.phtml:162
msgid "仓位"
msgstr ""

#: ../../view/layout/left-menu.phtml:98
msgid "库存盘点"
msgstr ""

#: ../../view/layout/left-menu.phtml:103
msgid "库间调拨"
msgstr ""

#: ../../view/layout/left-menu.phtml:119
#: ../../view/partial/breadcrumb.phtml:221
#: ../../view/partial/breadcrumb.phtml:257
msgid "采购订单"
msgstr ""

#: ../../view/layout/left-menu.phtml:124
msgid "采购入库单"
msgstr ""

#: ../../view/layout/left-menu.phtml:129
#: ../../view/partial/breadcrumb.phtml:242
msgid "采购退货单"
msgstr ""

#: ../../view/layout/left-menu.phtml:145
#: ../../view/partial/breadcrumb.phtml:398
msgid "销售订单"
msgstr ""

#: ../../view/layout/left-menu.phtml:150
#: ../../view/partial/breadcrumb.phtml:419
msgid "销售发货单"
msgstr ""

#: ../../view/layout/left-menu.phtml:155
#: ../../view/partial/breadcrumb.phtml:431
msgid "销售退货单"
msgstr ""

#: ../../view/layout/left-menu.phtml:170
#: ../../view/partial/breadcrumb.phtml:451
msgid "订单"
msgstr ""

#: ../../view/layout/left-menu.phtml:191
#: ../../view/partial/breadcrumb.phtml:298
msgid "客户分类"
msgstr ""

#: ../../view/layout/left-menu.phtml:196
#: ../../view/partial/breadcrumb.phtml:316
msgid "供应商"
msgstr ""

#: ../../view/layout/left-menu.phtml:201
#: ../../view/partial/breadcrumb.phtml:334
msgid "供应商分类"
msgstr ""

#: ../../view/layout/left-menu.phtml:225
msgid "其他应付账款"
msgstr ""

#: ../../view/layout/left-menu.phtml:230
msgid "其他应收账款"
msgstr ""

#: ../../view/partial/breadcrumb.phtml:16
msgid "管理员列表"
msgstr ""

#: ../../view/partial/breadcrumb.phtml:25
msgid "删除管理员"
msgstr ""

#: ../../view/partial/breadcrumb.phtml:37
msgid "管理员组列表"
msgstr ""

#: ../../view/partial/breadcrumb.phtml:46
msgid "删除管理员组"
msgstr ""

#: ../../view/partial/breadcrumb.phtml:55
msgid "地区列表"
msgstr ""

#: ../../view/partial/breadcrumb.phtml:64
msgid "删除地区"
msgstr ""

#: ../../view/partial/breadcrumb.phtml:73
msgid "商城列表"
msgstr ""

#: ../../view/partial/breadcrumb.phtml:79
msgid "编辑应用"
msgstr ""

#: ../../view/partial/breadcrumb.phtml:82
msgid "删除商城"
msgstr ""

#: ../../view/partial/breadcrumb.phtml:91
msgid "日志列表"
msgstr ""

#: ../../view/partial/breadcrumb.phtml:106
msgid "商品列表"
msgstr ""

#: ../../view/partial/breadcrumb.phtml:109
msgid "添加商品"
msgstr ""

#: ../../view/partial/breadcrumb.phtml:112
msgid "编辑商品"
msgstr ""

#: ../../view/partial/breadcrumb.phtml:115
msgid "删除商品"
msgstr ""

#: ../../view/partial/breadcrumb.phtml:118
msgid "采购价格趋势"
msgstr ""

#: ../../view/partial/breadcrumb.phtml:121
msgid "商品分布"
msgstr ""

#: ../../view/partial/breadcrumb.phtml:130
msgid "商品分类列表"
msgstr ""

#: ../../view/partial/breadcrumb.phtml:133
msgid "添加商品分类"
msgstr ""

#: ../../view/partial/breadcrumb.phtml:136
msgid "编辑商品分类"
msgstr ""

#: ../../view/partial/breadcrumb.phtml:139
msgid "删除商品分类"
msgstr ""

#: ../../view/partial/breadcrumb.phtml:148
msgid "品牌列表"
msgstr ""

#: ../../view/partial/breadcrumb.phtml:151
msgid "添加品牌"
msgstr ""

#: ../../view/partial/breadcrumb.phtml:154
msgid "编辑品牌"
msgstr ""

#: ../../view/partial/breadcrumb.phtml:157
msgid "删除品牌"
msgstr ""

#: ../../view/partial/breadcrumb.phtml:166
msgid "仓位列表"
msgstr ""

#: ../../view/partial/breadcrumb.phtml:169
msgid "添加仓位"
msgstr ""

#: ../../view/partial/breadcrumb.phtml:172
msgid "编辑仓位"
msgstr ""

#: ../../view/partial/breadcrumb.phtml:175
msgid "删除仓位"
msgstr ""

#: ../../view/partial/breadcrumb.phtml:184
msgid "单位列表"
msgstr ""

#: ../../view/partial/breadcrumb.phtml:187
msgid "添加单位"
msgstr ""

#: ../../view/partial/breadcrumb.phtml:190
msgid "编辑单位"
msgstr ""

#: ../../view/partial/breadcrumb.phtml:193
msgid "删除单位"
msgstr ""

#: ../../view/partial/breadcrumb.phtml:202
msgid "仓库列表"
msgstr ""

#: ../../view/partial/breadcrumb.phtml:205
msgid "添加仓库"
msgstr ""

#: ../../view/partial/breadcrumb.phtml:208
msgid "编辑仓库"
msgstr ""

#: ../../view/partial/breadcrumb.phtml:211
msgid "删除仓库"
msgstr ""

#: ../../view/partial/breadcrumb.phtml:225
msgid "采购订单列表"
msgstr ""

#: ../../view/partial/breadcrumb.phtml:228
msgid "添加采购订单"
msgstr ""

#: ../../view/partial/breadcrumb.phtml:231
msgid "编辑采购订单"
msgstr ""

#: ../../view/partial/breadcrumb.phtml:234
msgid "查看采购订单"
msgstr ""

#: ../../view/partial/breadcrumb.phtml:237
msgid "删除采购订单"
msgstr ""

#: ../../view/partial/breadcrumb.phtml:246
msgid "采购退货单列表"
msgstr ""

#: ../../view/partial/breadcrumb.phtml:249
msgid "采购退货详情"
msgstr ""

#: ../../view/partial/breadcrumb.phtml:252
msgid "采购退货"
msgstr ""

#: ../../view/partial/breadcrumb.phtml:261
msgid "采购入库单列表"
msgstr ""

#: ../../view/partial/breadcrumb.phtml:264
msgid "添加采购入库单"
msgstr ""

#: ../../view/partial/breadcrumb.phtml:267
msgid "编辑采购入库单"
msgstr ""

#: ../../view/partial/breadcrumb.phtml:270
msgid "删除采购入库单"
msgstr ""

#: ../../view/partial/breadcrumb.phtml:284
msgid "客户列表"
msgstr ""

#: ../../view/partial/breadcrumb.phtml:287
msgid "添加客户"
msgstr ""

#: ../../view/partial/breadcrumb.phtml:290
msgid "编辑客户"
msgstr ""

#: ../../view/partial/breadcrumb.phtml:293
msgid "删除客户"
msgstr ""

#: ../../view/partial/breadcrumb.phtml:302
msgid "客户分类列表"
msgstr ""

#: ../../view/partial/breadcrumb.phtml:305
msgid "添加客户分类"
msgstr ""

#: ../../view/partial/breadcrumb.phtml:308
msgid "编辑客户分类"
msgstr ""

#: ../../view/partial/breadcrumb.phtml:311
msgid "删除客户分类"
msgstr ""

#: ../../view/partial/breadcrumb.phtml:320
msgid "供应商列表"
msgstr ""

#: ../../view/partial/breadcrumb.phtml:323
msgid "添加供应商"
msgstr ""

#: ../../view/partial/breadcrumb.phtml:326
msgid "编辑供应商"
msgstr ""

#: ../../view/partial/breadcrumb.phtml:329
msgid "删除供应商"
msgstr ""

#: ../../view/partial/breadcrumb.phtml:338
msgid "供应商分类列表"
msgstr ""

#: ../../view/partial/breadcrumb.phtml:341
msgid "添加供应商分类"
msgstr ""

#: ../../view/partial/breadcrumb.phtml:344
msgid "编辑供应商分类"
msgstr ""

#: ../../view/partial/breadcrumb.phtml:347
msgid "删除供应商分类"
msgstr ""

#: ../../view/partial/breadcrumb.phtml:362
msgid "应付账款列表"
msgstr ""

#: ../../view/partial/breadcrumb.phtml:365
msgid "应付账款详情"
msgstr ""

#: ../../view/partial/breadcrumb.phtml:368
msgid "付款操作"
msgstr ""

#: ../../view/partial/breadcrumb.phtml:371
msgid "付款记录"
msgstr ""

#: ../../view/partial/breadcrumb.phtml:379
msgid "应收账款列表"
msgstr ""

#: ../../view/partial/breadcrumb.phtml:382
msgid "应收账款详情"
msgstr ""

#: ../../view/partial/breadcrumb.phtml:385
msgid "收款操作"
msgstr ""

#: ../../view/partial/breadcrumb.phtml:388
msgid "收款记录"
msgstr ""

#: ../../view/partial/breadcrumb.phtml:402
msgid "销售订单列表"
msgstr ""

#: ../../view/partial/breadcrumb.phtml:405
msgid "销售订单详情"
msgstr ""

#: ../../view/partial/breadcrumb.phtml:408
msgid "添加销售订单"
msgstr ""

#: ../../view/partial/breadcrumb.phtml:411
msgid "编辑销售订单"
msgstr ""

#: ../../view/partial/breadcrumb.phtml:414
msgid "订单发货出库"
msgstr ""

#: ../../view/partial/breadcrumb.phtml:423
msgid "销售发货单列表"
msgstr ""

#: ../../view/partial/breadcrumb.phtml:426
msgid "发货订单详情"
msgstr ""

#: ../../view/partial/breadcrumb.phtml:435
msgid "销售退货单列表"
msgstr ""

#: ../../view/partial/breadcrumb.phtml:438
msgid "销售退货"
msgstr ""

#: ../../view/partial/breadcrumb.phtml:441
msgid "销售退货单详情"
msgstr ""

#: ../../view/partial/breadcrumb.phtml:455
msgid "订单列表"
msgstr ""

#: ../../view/partial/breadcrumb.phtml:458
msgid "商城订单详情"
msgstr ""
